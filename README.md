# SMILES2PropertiesTransformer

The SMILES2PropertiesTransformer has been developed by members of the EPSE group in the following paper:

> [Winter, B., Winter, C., Schilling, J., & Bardow, A. (2022). A smile is all you need: predicting limiting activity coefficients from SMILES with natural language processing. Digital Discovery](https://doi.org/10.1039/D2DD00058J)

You can find the corresponding Git here: 

https://github.com/Bene94/SMILES2PropertiesTransformer

